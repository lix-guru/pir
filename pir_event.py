#!/usr/bin/python

# PIR motion detection script
# HC-SR501 out pin connected to RPi pin 8, as input (3.3v / 0v)

import time
import datetime
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

motionPin = 8  #out pin of HC-SR501
GPIO.setup(motionPin, GPIO.IN, GPIO.PUD_DOWN)

def motionDetected(channel):
  print(str(datetime.datetime.now()), GPIO.input(channel))

GPIO.add_event_detect(motionPin, GPIO.BOTH, motionDetected, 500)

try:
  while(True):
    time.sleep(1)
except KeyboardInterrupt:
  GPIO.cleanup()

