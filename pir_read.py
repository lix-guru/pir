#!/usr/bin/python

# PIR motion detection script
# HC-SR501 out pin connected to RPi pin 8, as input (3.3v / 0v)

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

motionPin = 8  #out pin of HC-SR501
GPIO.setup(motionPin, GPIO.IN, GPIO.PUD_DOWN)

print(GPIO.input(motionPin))

GPIO.cleanup()

